package com.bootcamp.homework.service;

import com.bootcamp.homework.business.service.UserService;
import com.bootcamp.homework.business.service.exceptions.IncorrectEmail;
import com.bootcamp.homework.business.service.exceptions.UserAlreadyExists;
import com.bootcamp.homework.business.service.exceptions.UserDoesNotExists;
import com.bootcamp.homework.database.entity.User;
import com.bootcamp.homework.database.entity.UserData;
import com.bootcamp.homework.database.entity.UserScore;
import com.bootcamp.homework.database.repository.UserDataRepository;
import com.bootcamp.homework.database.repository.UserRepository;
import com.bootcamp.homework.database.repository.UserScoreRepository;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserDataRepository userDataRepository;

    @Mock
    private UserScoreRepository userScoreRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(passwordEncoder.encode(anyString())).thenReturn("testPass");
    }

    @Test
    public void testRegisterNewUser() throws UserAlreadyExists, IncorrectEmail {
        String inputUsername = "input";

        User user = new User();
        UserData userData = new UserData();

        String username = "input";
        String password = "testPass";
        String role = "ROLE_USER";
        String firstName = "firstName";
        String lastName = "lastName";
        String gender = "male";
        String email = "email@email.com";

        user.setUsername(username);
        user.setPassword(password);
        user.setRole(role);
        userData.setUsername(username);
        userData.setFirstName(firstName);
        userData.setLastName(lastName);
        userData.setGender(gender);
        userData.setEmail(email);
        user.setUserData(userData);

        when(userRepository.findByUsername(inputUsername)).thenReturn(null);
        when(userRepository.save(any(User.class))).thenReturn(user);
        when(userDataRepository.save(any(UserData.class))).thenReturn(userData);

        User userTest = userService.registerNewUser(username, password, firstName, lastName, gender, email);

        assertEquals(user.getUsername(), userTest.getUsername());
        assertEquals(user.getPassword(), userTest.getPassword());
        assertEquals(user.getUserData().getUsername(), userTest.getUserData().getUsername());
        assertEquals(user.getUserData().getFirstName(), userTest.getUserData().getFirstName());
        assertEquals(user.getUserData().getLastName(), userTest.getUserData().getLastName());
        assertEquals(user.getUserData().getLastName(), userTest.getUserData().getLastName());
        assertEquals(user.getUserData().getGender(), userTest.getUserData().getGender());
        assertEquals(user.getUserData().getEmail(), userTest.getUserData().getEmail());
    }

    @Test
    public void testGetUserData() throws UserDoesNotExists {
        String inputUsername = "input";
        String username = "input";

        UserData userData = new UserData();
        userData.setUsername(username);

        when(userDataRepository.findByUsername(inputUsername)).thenReturn(userData);

        UserData userDataTest = userService.getUserData(inputUsername);

        assertEquals(userData.getUsername(),userDataTest.getUsername());
    }

    @Test
    public void testModify() throws UserDoesNotExists, IncorrectEmail {
        User user = new User();

        String username = "input";
        String firstName = "firstName";
        String lastName = "lastName";
        String gender = "male";
        String email = "email@email.com";

        UserData userDataInit = new UserData();
        userDataInit.setUsername(username);

        UserData userDataResult = new UserData();
        userDataResult.setUsername(username);
        userDataResult.setFirstName(firstName);
        userDataResult.setLastName(lastName);
        userDataResult.setGender(gender);
        userDataResult.setEmail(email);

        ObjectNode jsNode = JsonNodeFactory.instance.objectNode();
        jsNode.put("username", username);
        jsNode.put("firstName", firstName);
        jsNode.put("lastName", lastName);
        jsNode.put("gender", gender);
        jsNode.put("email@email.com", email);

        when(userDataRepository.findByUsername(username)).thenReturn(userDataInit);
        when(userDataRepository.save(any(UserData.class))).thenReturn(userDataResult);
        when(userRepository.findByUsername(username)).thenReturn(user);

        UserData userDataTest = userService.modify(username, jsNode);

        assertEquals(userDataResult.getUsername(), userDataTest.getUsername());
        assertEquals(userDataResult.getFirstName(), userDataTest.getFirstName());
        assertEquals(userDataResult.getLastName(), userDataTest.getLastName());
        assertEquals(userDataResult.getLastName(), userDataTest.getLastName());
        assertEquals(userDataResult.getGender(), userDataTest.getGender());
        assertEquals(userDataResult.getEmail(), userDataTest.getEmail());
    }

    @Test
    public void testRegisterScore() {
        UserScore userScore = new UserScore();
        String username = "username";
        String game = "ballGame";
        String score = "15";
        String life = "3";

        userScore.setUsername(username);
        userScore.setGame(game);
        userScore.setScore(score);
        userScore.setLife(life);

        ObjectNode jsNode = JsonNodeFactory.instance.objectNode();
        jsNode.put("username", username);
        jsNode.put("game", game);
        jsNode.put("score", score);
        jsNode.put("life", life);

        when(userScoreRepository.save(any(UserScore.class))).thenReturn(userScore);

        UserScore userScoreTest = userService.registerScore(username, jsNode);

        assertEquals(userScore.getUsername(), userScoreTest.getUsername());
        assertEquals(userScore.getGame(), userScoreTest.getGame());
        assertEquals(userScore.getScore(), userScoreTest.getScore());
        assertEquals(userScore.getLife(), userScoreTest.getLife());

    }

    @Test
    public void testGetUserScore() {
        List<UserScore> userScores = new ArrayList<>();

        for(int i = 0; i < 20; i++) {
            UserScore userScore = new UserScore();
            userScore.setGame("ballGame");
            userScore.setScore(Integer.toString(i));
            userScore.setLife(Integer.toString(i%3));
            userScores.add(userScore);
        }

        when(userScoreRepository.findAll()).thenReturn(userScores);

        List<UserScore> userScoresTest = userService.getUserScore();

        for(int i = 0; i < 10; i++) {
            assertEquals(userScores.get(19-i), userScoresTest.get(i));
        }
    }

    @Test(expected = UserAlreadyExists.class)
    public void testRegisterNewUserExceptionUserAlreadyExists() throws UserAlreadyExists, IncorrectEmail {
        String username = "input";
        String password = "testPass";
        String firstName = "firstName";
        String lastName = "lastName";
        String gender = "male";
        String email = "email@email.com";

        User user = new User();

        when(userRepository.findByUsername(username)).thenReturn(user);

        User userTest = userService.registerNewUser(username, password, firstName, lastName, gender, email);
    }

    @Test(expected = IncorrectEmail.class)
    public void testRegisterNewUserExceptionIncorrectEmail() throws UserAlreadyExists, IncorrectEmail {
        String username = "input";
        String password = "testPass";
        String firstName = "firstName";
        String lastName = "lastName";
        String gender = "male";
        String email = "email@email";

        User user = new User();

        when(userRepository.findByUsername(username)).thenReturn(null);

        User userTest = userService.registerNewUser(username, password, firstName, lastName, gender, email);
    }

    @Test(expected = UserDoesNotExists.class)
    public void testGetUserDataException() throws UserDoesNotExists {
        String username = "input";

        when(userDataRepository.findByUsername(username)).thenReturn(null);

        UserData userDataTest = userService.getUserData(username);
    }

    @Test(expected = UserDoesNotExists.class)
    public void testModifyExceptionUserDoesNotExists() throws UserDoesNotExists, IncorrectEmail {
        String username = "input";

        ObjectNode jsNode = JsonNodeFactory.instance.objectNode();
        jsNode.put("firstName","firstName");

        when(userDataRepository.findByUsername(username)).thenReturn(null);

        UserData userData = userService.modify(username, jsNode);
    }

    @Test(expected = UserDoesNotExists.class)
    public void testModifyExceptionIncorrectEmail() throws UserDoesNotExists, IncorrectEmail {
        String username = "input";

        ObjectNode jsNode = JsonNodeFactory.instance.objectNode();
        jsNode.put("firstName","firstName");
        jsNode.put("email","email@email");

        when(userDataRepository.findByUsername(username)).thenReturn(null);

        UserData userData = userService.modify(username, jsNode);
    }
}
