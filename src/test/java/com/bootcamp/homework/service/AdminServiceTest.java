package com.bootcamp.homework.service;

import com.bootcamp.homework.business.service.AdminService;
import com.bootcamp.homework.business.service.UserService;
import com.bootcamp.homework.business.service.exceptions.UserDoesNotExists;
import com.bootcamp.homework.database.entity.User;
import com.bootcamp.homework.database.entity.UserData;
import com.bootcamp.homework.database.repository.UserDataRepository;
import com.bootcamp.homework.database.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AdminServiceTest {

    @InjectMocks
    private AdminService adminService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserDataRepository userDataRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testChangeRole() throws UserDoesNotExists {
        User userInit = new User();
        User user = new User();
        String username = "input";
        String role = "ROLE_USER";

        userInit.setUsername(username);
        user.setUsername(username);
        user.setRole(role);

        when(userRepository.findByUsername(username)).thenReturn(userInit);
        when(userRepository.save(any(User.class))).thenReturn(user);

        User userTest = adminService.changeRole(username, role);

        assertEquals(user.getUsername(), userTest.getUsername());
        assertEquals(user.getRole(), userTest.getRole());
    }

    @Test
    public void testDeleteByUsername() throws UserDoesNotExists {
        User user = new User();
        UserData userData = new UserData();

        String username = "input";
        user.setUsername(username);
        userData.setUsername(username);

        when(userRepository.findByUsername(username)).thenReturn(user);
        when(userDataRepository.findByUsername(username)).thenReturn(userData);

        adminService.deleteByUsername(username);

        verify(userRepository).delete(user);
        verify(userDataRepository).delete(userData);
    }

    @Test(expected = UserDoesNotExists.class)
    public void testChangeRoleException() throws UserDoesNotExists {
        String username = "input";
        String role = "ROLE_ADMIN";

        when(userRepository.findByUsername(username)).thenReturn(null);

        User user = adminService.changeRole(username, role);
    }

    @Test(expected = UserDoesNotExists.class)
    public void testDeleteByUsernameException() throws UserDoesNotExists {
        String username = "input";

        when(userRepository.findByUsername(username)).thenReturn(null);
        when(userDataRepository.findByUsername(username)).thenReturn(null);

        adminService.deleteByUsername(username);
    }

}
