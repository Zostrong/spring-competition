$("form").submit( function() {
    var email = $('#email').val();

    if(email != "") {
         if(!validateForm(email)) {
             $('#emailFailedMessage').show();
             return false;
         }
         $('#emailFailedMessage').hide();
         return true;
    };
});


