(function() {
    $(document).ready(function() {
        getRanklist();
    })
})();

function getRanklist() {
     $.ajax({
         url : "/getranklist",
         async : true,
         cache : false,
         timeout : 5000,

         success : function(response) {
              $("#userScores").hide();
              $('#ranklist').find("tr[id^='rank']").remove();

              for(var i = 0; i < response.ranklist.length; i++) {
                   var username = response.ranklist[i].username;
                   var score = response.ranklist[i].score;
                   var life = response.ranklist[i].life;

                   var ranklist = $('#userScores').clone().appendTo('#ranklist');

                   var ranklistRank = ranklist.find("#rank");
                   var ranklistUsername = ranklist.find('#username');
                   var ranklistScore = ranklist.find('#score');
                   var ranklistLives = ranklist.find('#lives');

                   ranklistRank.html(i + 1);
                   ranklistUsername.html(username);
                   ranklistScore.html(score);
                   ranklistLives.html(life);

                   ranklist.attr("id","rank" + i);
                   ranklistUsername.attr("id","username" + i);
                   ranklistScore.attr("id","score" + i);
                   ranklistLives.attr("id","life" + i);

                   ranklist.show();
              }
         },

         error : function(XMLHttpRequest, textStatus, errorThrown) {
              console.log("ranklist retrieval failed... HTTP status code: " + XMLHttpRequest.status + ' message ' + XMLHttpRequest.responseText);
              $('#loadFailedMsg').show();
         }
    });
}