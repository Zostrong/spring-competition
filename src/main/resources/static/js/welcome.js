(function() {
    $(document).ready(function() {
        welcomeUser();
    })
})();

function welcomeUser() {
    $.ajax({
        url : '/welcome/user',
        method: 'GET',
        async : true,
        cache : false,
        timeout : 5000,

        success : function(responseText) {
            $('body').append($('<h1>Welcome ' + responseText + '!</h1>'));
        },

        error : function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("Receiving active user failed... HTTP status code: " + XMLHttpRequest.status + ' message ' + XMLHttpRequest.responseText);
        }
    });
}