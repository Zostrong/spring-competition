$("#descButton").css("cursor","pointer");
var ids = ["#username", "#firstName", "#lastName", "#gender", "#email"];

(function() {
    $(document).ready(function() {
        getUserDetails();
    })
})();

function getUserDetails() {
    var user = location.hash.substr(1);

     $.ajax({
          url : "/admin/description/" + user,
          async : true,
          cache : false,
          timeout : 5000,

          data : {},

          success : function(response) {

                var userDataInfo = [response.userData.username, response.userData.firstName, response.userData.lastName,
                                                    response.userData.gender, response.userData.email];

                for(var i = 0; i < ids.length; i++) {
                    $(ids[i]).val(userDataInfo[i]);
                    $(ids[i]).prop('disabled', true);
                }
          },

          error : function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("Receiving user description failed... HTTP status code: " + XMLHttpRequest.status + ' message ' + XMLHttpRequest.responseText);
            $('#loadFailedMsg').show();
          }
     });
}

$("#editAdmin").on('click', function(e) {

    for(var i = 0; i < ids.length; i++) {
        $(ids[i]).prop('disabled', false);
    }

    $('.message').hide();
    $('.error-message').hide();
    $("#editAdmin").hide();
    $("#stopEditAdmin").show();

});

$("#stopEditAdmin").click('click', function() {
    var fields = {};
    $("#originalUserPropInput").find(".field").each(function() {
    fields[$(this).attr("id")] = $(this).val();
    });
    var descriptionJSON = JSON.stringify(fields);

    $.ajax({
          type: "POST",
          url: "/admin/modify",
          data: descriptionJSON,
          dataType: "json",
          contentType : "application/json",

          success: function(e) {
              for(var i = 0; i < ids.length; i++) {
                $(ids[i]).prop('disabled', true);
              }

              $('#modifyMsg').show();
              $("#editAdmin").show();
              $("#stopEditAdmin").hide();
          },

          error : function(XMLHttpRequest, textStatus, errorThrown) {
              console.log("Modifying user failed... HTTP status code: " + XMLHttpRequest.status + ' message ' + XMLHttpRequest.responseText);
              $('#modifyFailedMsg').show();
          }
    });

    return false;

});