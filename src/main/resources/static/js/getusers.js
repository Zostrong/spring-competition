var update = false;

(function() {
    $(document).ready(function() {
        getUsers();
    })
})();

$("#edit").on('click', function(e) {
    $(".message").hide();
    $(".error-message").hide();
    $(".selectRole").show();
    $(".deleteButton").show();

    $("#stopEdit").show();
    $("#edit").hide();
    $('.updatedRole').hide();

    update = true;
});

$("#stopEdit").on('click', function(e) {
    $(".message").hide();
    $(".error-message").hide();
    $(".selectRole").hide();
    $(".deleteButton").hide();

    $("#stopEdit").hide();
    $("#edit").show();

    $('.updatedRole').show();
});

function changeRole(element) {
    var user = $(element).attr('user');
    var role = $(element).attr('role');
    $(".message").hide();

    $.ajax({
         url : '/admin/userlist/' + encodeURIComponent(user) + '-' + encodeURIComponent(role),
         method: 'POST',
         async : true,
         cache : false,
         timeout : 5000,

         success : function() {
             $('#modifyMsg').show();
             getUsers();
         },

         error : function(XMLHttpRequest, textStatus, errorThrown) {
             console.log("Changing user role failed... HTTP status code: " + XMLHttpRequest.status + ' message ' + XMLHttpRequest.responseText);
             $('#modifyFailedMsg').show();
         }
    });
}

function deleteUser(element) {
     var user = $(element).attr('user');
     $(".message").hide();

    $.ajax({
         url : '/admin/userlist/' + encodeURIComponent(user),
         method: 'DELETE',
         async : true,
         cache : false,
         timeout : 5000,

         success : function() {
             $('#deleteMsg').show();
             getUsers();
         },

         error : function(XMLHttpRequest, textStatus, errorThrown) {
             console.log("Deleting user failed... HTTP status code: " + XMLHttpRequest.status + ' message ' + XMLHttpRequest.responseText);
             $('#deleteFailedMsg').show();
         }
    });
}

function getUsers() {
     $.ajax({
      url : "/admin/getusers",
      async : true,
      cache : false,
      timeout : 5000,

      success : function(response) {
            $("#originalUserProp").hide();
            $('#userList').find("tr[id^='user']").remove();

            for(var i = 0; i < response.userlist.length; i++) {
                var username = response.userlist[i].username;
                var password = response.userlist[i].password;
                var role = response.userlist[i].role;

                var userProp = $('#originalUserProp').clone().appendTo('#userList');

                var userPropUsername = userProp.find('#username')
                var userPropPassword = userProp.find('#password')
                var userPropActRole = userProp.find('#actRole')
                var userPropButton = userProp.find('#deleteButton');

                var userPropRole = userProp.find('#role')

                userPropUsername.html(username);
                userPropPassword.html(password);

                userPropActRole.html(role == 'ROLE_ADMIN' ? "admin" : "user");
                userPropRole.val(role == 'ROLE_ADMIN' ? "admin" : "user");

                userPropButton.attr('user', username);
                userPropButton.on('click', function(e) {
                    deleteUser(this);
                });

                userPropRole.attr('user', username);
                userPropRole.change(function(e) {
                    var actualRole = $(this).val();
                    $(this).attr('role', actualRole);
                    changeRole(this);
                });

                userPropUsername.attr('user', username);
                userPropUsername.on('click', function(e) {
                    navigateToSelectedUser(this);
                });

                userProp.attr("id","user" + i);
                userPropUsername.attr("id","username" + i);
                userPropPassword.attr("id","password" + i);
                userPropActRole.attr("class","updatedRole");
                userPropRole.attr("id","role" + i);

                if(update) {
                    $('.updatedRole').hide();
                }
                userProp.show();
            }
      },

      error : function(XMLHttpRequest, textStatus, errorThrown) {
        console.log("userList retrieval failed... HTTP status code: " + XMLHttpRequest.status + ' message ' + XMLHttpRequest.responseText);
        $('#loadFailedMsg').show();
      }
    });
}

function navigateToSelectedUser(element) {
    var user = $(element).attr('user');
    window.location = "/admin/admindescription#" + encodeURIComponent(user);
}

