package com.bootcamp.homework.business.service;

import com.bootcamp.homework.business.service.exceptions.UserDoesNotExists;
import com.bootcamp.homework.database.entity.Roles;
import com.bootcamp.homework.database.entity.User;
import com.bootcamp.homework.database.entity.UserData;
import com.bootcamp.homework.database.repository.UserDataRepository;
import com.bootcamp.homework.database.repository.UserRepository;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdminService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDataRepository userDataRepository;

    public List<User> getUsers() {
        List<User> userList = new ArrayList<>();
        Iterable<User> users = userRepository.findAll();

        users.forEach(user -> userList.add(user));

        return userList;
    }

    public User changeRole(String username, String newRole) throws UserDoesNotExists {
        User user = userRepository.findByUsername(username);

        if(user != null) {
            user.setRole(newRole.equals("admin") ? Roles.ADMIN.type() : Roles.USER.type());
            return userRepository.save(user);
        } else {
            throw new UserDoesNotExists("User does not exists");
        }

    }

    public void deleteByUsername(String username) throws UserDoesNotExists {
        User user = userRepository.findByUsername(username);
        UserData userData = userDataRepository.findByUsername(username);

        if(user != null) {
            userRepository.delete(user);
            userDataRepository.delete(userData);
        } else {
            throw new UserDoesNotExists("User does not exists");
        }
    }

    public User getSelectedUsername(JsonNode jsonNode) {
        User userTemp = new User();

        jsonNode.fields().forEachRemaining((field) -> {
            String key = field.getKey();
            String value = field.getValue().asText();

            if(key.equals("username")) {
                userTemp.setUsername(value);
            }
        });

        return userTemp;
    }



}
