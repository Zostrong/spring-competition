package com.bootcamp.homework.business.service.exceptions;

public class UserDoesNotExists extends DataBaseException {

    public UserDoesNotExists(String s) {
        super(s);
    }
}
