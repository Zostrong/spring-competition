package com.bootcamp.homework.business.service;

import com.bootcamp.homework.business.service.exceptions.IncorrectEmail;
import com.bootcamp.homework.business.service.exceptions.UserAlreadyExists;
import com.bootcamp.homework.business.service.exceptions.UserDoesNotExists;
import com.bootcamp.homework.database.entity.User;
import com.bootcamp.homework.database.entity.UserData;
import com.bootcamp.homework.database.entity.UserScore;
import com.bootcamp.homework.database.repository.UserDataRepository;
import com.bootcamp.homework.database.repository.UserRepository;
import com.bootcamp.homework.database.repository.UserScoreRepository;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDataRepository userDataRepository;

    @Autowired
    private UserScoreRepository userScoreRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private boolean emailValidation = true;

    public User registerNewUser(String username, String password, String firstName, String lastName, String gender, String email) throws UserAlreadyExists, IncorrectEmail {

        User user = userRepository.findByUsername(username);

        if(user == null) {
            //Mandatory
            user = new User();
            UserData userData = new UserData();

            user.setUsername(username);
            user.setPassword(passwordEncoder.encode(password));
            user.setRole("ROLE_USER");
            userData.setUsername(username);

            //Not Mandatory
            userData.setFirstName(firstName);
            userData.setLastName(lastName);
            userData.setGender(gender);
            if(!email.equals("")) {
                EmailValidator emailValidator = EmailValidator.getInstance();
                if(emailValidator.isValid(email)) {
                    userData.setEmail(email);
                } else {
                    throw new IncorrectEmail("Incorrect email format");
                }
            }

            user.setUserData(userData);

            userDataRepository.save(userData);
            return userRepository.save(user);

        } else {
            throw new UserAlreadyExists("User already exists");
        }
    }

    public UserData getUserData(String username) throws UserDoesNotExists {

        UserData userData = userDataRepository.findByUsername(username);

        if(userData != null) {
            return userData;
        } else {
            throw new UserDoesNotExists("User does not exists");
        }


    }

    public UserData modify(String username, JsonNode jsonNode) throws UserDoesNotExists, IncorrectEmail {

        UserData userData = userDataRepository.findByUsername(username);

        if(userData != null) {
            jsonNode.fields().forEachRemaining((field) -> {
                String key = field.getKey();
                String value = field.getValue().asText();

                switch (key) {
                    case "username":
                        User user = userRepository.findByUsername(username);

                        userData.setUsername(value);
                        user.setUsername(value);
                        break;
                    case "firstName":
                        userData.setFirstName(value);
                        break;
                    case "lastName":
                        userData.setLastName(value);
                        break;
                    case "gender":
                        userData.setGender(value);
                        break;
                    case "email":
                        String email = value;
                        if(!email.equals("")) {
                            EmailValidator emailValidator = EmailValidator.getInstance();
                            if(emailValidator.isValid(email)) {
                                userData.setEmail(email);
                            } else {
                                setEmailValidation(false);
                            }
                        }
                        break;
                    default:
                }

            });

            if(!emailValidation) {
                setEmailValidation(true);
                throw new IncorrectEmail("Incorrect email format");
            }

            return userDataRepository.save(userData);
        } else {
            throw new UserDoesNotExists("User does not exists");
        }
    }

    public UserScore registerScore(String username, JsonNode jsonNode) {
        UserScore userScore = new UserScore();
        userScore.setUsername(username);

        jsonNode.fields().forEachRemaining(field -> {
            String key = field.getKey();
            String value = field.getValue().asText();

            switch (key) {
                case "game":
                    userScore.setGame(value);
                    break;
                case "life":
                    userScore.setLife(value);
                    break;
                case "score":
                    userScore.setScore(value);
                    break;
                default:
            }
        });

        return userScoreRepository.save(userScore);
    }

    public List<UserScore> getUserScore() {
        List<UserScore> userScoreList = new ArrayList<>();
        List<UserScore> ranklist = new ArrayList<>();
        Iterable<UserScore> userScores = userScoreRepository.findAll();

        userScores.forEach(userScore -> userScoreList.add(userScore));

        ranklist = userScoreList.stream()
                .sorted((score1, score2) -> {
                        int c;
                        c = score2.getIntScore().compareTo(score1.getIntScore());

                        if (c == 0) {
                            c = score2.getIntLife().compareTo(score1.getIntLife());
                        }
                        return c;
                    })
                .limit(10)
                .collect(Collectors.toList());

        return ranklist;
    }

    private void setEmailValidation(boolean value) {
        emailValidation = value;
    }
}
