package com.bootcamp.homework.business.service.exceptions;

public class UserAlreadyExists extends DataBaseException {

    public UserAlreadyExists(String s) {
        super(s);
    }
}
