package com.bootcamp.homework.business.service.exceptions;

public class IncorrectEmail extends DataBaseException {

    public IncorrectEmail(String s) {
        super(s);
    }
}
