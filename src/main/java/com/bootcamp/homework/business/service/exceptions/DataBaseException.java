package com.bootcamp.homework.business.service.exceptions;

public class DataBaseException extends Exception {

    public DataBaseException(String s) {
        super(s);
    }
}
