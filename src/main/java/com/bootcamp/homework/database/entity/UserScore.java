package com.bootcamp.homework.database.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class UserScore {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "USER_ID")
    private long id;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "GAME")
    private String game;

    @Column(name = "LIFE")
    private String life;

    @Column(name = "SCORE")
    private String score;

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLife() {
        return life;
    }

    public void setLife(String life) {
        this.life = life;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public Integer getIntScore() {
        return Integer.parseInt(score);
    }

    public Integer getIntLife() {
        return Integer.parseInt(life);
    }
}
