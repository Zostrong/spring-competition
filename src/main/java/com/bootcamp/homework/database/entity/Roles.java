package com.bootcamp.homework.database.entity;

public enum Roles {
    ADMIN,
    USER;

    public String type() {

        switch (this) {
            case ADMIN:
                return "ROLE_ADMIN";
            case USER:
                return "ROLE_USER";
            default:
                return "";
        }
    }

}
