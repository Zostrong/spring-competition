package com.bootcamp.homework.database;
import com.bootcamp.homework.business.service.AdminService;
import com.bootcamp.homework.business.service.UserService;
import com.bootcamp.homework.business.service.exceptions.DataBaseException;
import com.bootcamp.homework.business.service.exceptions.UserAlreadyExists;
import com.bootcamp.homework.business.service.exceptions.UserDoesNotExists;
import com.bootcamp.homework.database.entity.User;
import com.bootcamp.homework.database.entity.UserData;
import com.bootcamp.homework.database.entity.UserScore;
import com.bootcamp.homework.database.repository.UserDataRepository;
import com.bootcamp.homework.database.repository.UserRepository;
import com.bootcamp.homework.database.repository.UserScoreRepository;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

//Only for populating database with data, but if it has any default admin or user, it will not execute

@Component
public class  initDb {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private AdminService adminService;

    @PostConstruct
    public void init() {

        if(userRepository.findByUsername("admin") == null) {
            try {
                userService.registerNewUser("admin", "admin",
                        "firstAdmin", "lastAdmin", "male", "admin@admin.com");
                adminService.changeRole("admin", "admin");

                for(int i = 0; i < 5; i++) {
                    String username = "user" + i;
                    String password = "user" + i;
                    String firstName = "firstTeszt" + i;
                    String lastName = "lastTeszt" + i;
                    String gender = "male";
                    String email = "email@email" + i + ".com";

                    userService.registerNewUser(username, password, firstName, lastName, gender, email);
                }

            } catch (DataBaseException dataBaseException) {}
        }
    }
}

