package com.bootcamp.homework.database.repository;

import com.bootcamp.homework.database.entity.UserScore;
import org.springframework.data.repository.CrudRepository;

public interface UserScoreRepository extends CrudRepository<UserScore, Long> {
    UserScore findByUsername(String username);
    UserScore findById(Long id);
}
