package com.bootcamp.homework.database.repository;

import com.bootcamp.homework.database.entity.UserData;
import org.springframework.data.repository.CrudRepository;

public interface UserDataRepository extends CrudRepository<UserData, Long> {
    UserData findByUsername(String username);
}
