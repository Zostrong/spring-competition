package com.bootcamp.homework.database.repository;

import com.bootcamp.homework.database.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);
    void delete(User user);
}
