package com.bootcamp.homework.web;

import com.bootcamp.homework.database.entity.User;
import com.bootcamp.homework.database.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.Map;

@Controller
public class LoginController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homeInit() {
        return "welcomeInit";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String welcome(Principal principal) {
        User user = userRepository.findByUsername(principal.getName());

        if(user.getRole().equals("ROLE_ADMIN")) {
            return "adminWelcome";
        }

        return "userWelcome";
    }

    @RequestMapping(value = "/sorry", method = RequestMethod.GET)
    public String sorry() {
        return "sorry";
    }

    @RequestMapping(value = "/welcome/user", method = RequestMethod.GET)
    @ResponseBody
    public String welcomeUser(Principal principal) {
        return principal.getName();
    }
}
