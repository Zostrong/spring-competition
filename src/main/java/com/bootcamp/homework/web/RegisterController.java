package com.bootcamp.homework.web;

import com.bootcamp.homework.business.service.UserService;
import com.bootcamp.homework.business.service.exceptions.DataBaseException;
import com.bootcamp.homework.business.service.exceptions.IncorrectEmail;
import com.bootcamp.homework.business.service.exceptions.UserAlreadyExists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RegisterController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registration() {
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String add(@RequestParam String username,
                      @RequestParam String password,
                      @RequestParam String firstName,
                      @RequestParam String lastName,
                      @RequestParam String gender,
                      @RequestParam String email) {

        try {
            userService.registerNewUser(username, password, firstName, lastName, gender, email);
        } catch (DataBaseException DataBaseException) {
            return "errorPage";
        }

        return "add";
    }

}
