package com.bootcamp.homework.web;

import com.bootcamp.homework.business.service.UserService;
import com.bootcamp.homework.business.service.exceptions.DataBaseException;
import com.bootcamp.homework.business.service.exceptions.IncorrectEmail;
import com.bootcamp.homework.business.service.exceptions.UserDoesNotExists;
import com.bootcamp.homework.database.entity.UserData;
import com.bootcamp.homework.database.entity.UserScore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/user/description", method = RequestMethod.GET)
    public String userDescription() {
        return "userDescription";
    }

    @RequestMapping(value = "/user/game", method = RequestMethod.GET)
    public String game() {
        return "game";
    }

    @RequestMapping(value = "/user/ranklist", method = RequestMethod.GET)
    public String ranklist() {
        return "userRanklist";
    }

    @RequestMapping(value = "/user/getdescription", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> currentUserName(Principal principal) {
        Map<String, Object> userDataMap = new HashMap<>();

        String username = principal.getName();
        UserData userData = null;
        try {
            userData = userService.getUserData(username);
        } catch (UserDoesNotExists userDoesNotExists) {
            return null;
        }

        userDataMap.put("userData", userData);
        return userDataMap;
    }

    @RequestMapping(value = "/user/modify", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Void> modify(Principal principal, @RequestBody String body) throws DataBaseException {
        String username = principal.getName();

        try {
            JsonNode jsonNode = objectMapper.readTree(body);
            userService.modify(username, jsonNode);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (DataBaseException DataBaseException) {
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/user/savescore", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Void> saveScore(Principal principal, @RequestBody String body) {
        String username = principal.getName();

        try {
            JsonNode jsonNode = objectMapper.readTree(body);
            userService.registerScore(username, jsonNode);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/getranklist", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getRanklist() {
        Map<String, Object> ranklist = new HashMap<>();
        List<UserScore> userScores = userService.getUserScore();

        ranklist.put("ranklist", userScores);

        return ranklist;
    }
}
