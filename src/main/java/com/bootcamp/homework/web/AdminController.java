package com.bootcamp.homework.web;

import com.bootcamp.homework.business.service.AdminService;
import com.bootcamp.homework.business.service.UserService;
import com.bootcamp.homework.business.service.exceptions.DataBaseException;
import com.bootcamp.homework.business.service.exceptions.IncorrectEmail;
import com.bootcamp.homework.business.service.exceptions.UserDoesNotExists;
import com.bootcamp.homework.database.entity.User;
import com.bootcamp.homework.database.entity.UserData;
import com.bootcamp.homework.database.entity.UserScore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class AdminController {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private UserService userService;

    @Autowired
    private AdminService adminService;

    //Responsible for loading page

    @RequestMapping(value = "/admin/userlist", method = RequestMethod.GET)
    public String userlist() {
        return "userlist";
    }

    @RequestMapping(value = "/admin/admindescription", method = RequestMethod.GET)
    public String adminDescription() {
        return "adminDescription";
    }

    @RequestMapping(value = "/admin/ranklist", method = RequestMethod.GET)
    public String ranklist() {
        return "adminRanklist";
    }


    //Responsible for sending correct Json

    @RequestMapping(value = "/admin/getusers", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getUsers() {
        List <User> userList = adminService.getUsers();
        Map<String, Object> userMap = new HashMap<>();

        userMap.put("userlist", userList);

        return userMap;
    }

    @RequestMapping(value = "/admin/description/{user}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getDescription(@PathVariable("user") String username) {
        UserData userData = null;
        try {
            userData = userService.getUserData(username);
        } catch (UserDoesNotExists userDoesNotExists) {
            return null;
        }

        Map<String, Object> userDataMap = new HashMap<>();

        userDataMap.put("userData", userData);
        return userDataMap;
    }

    //Responsible for changing data in db

    @RequestMapping(value = "/admin/userlist/{username}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUser(@PathVariable("username") String username) {
        try {
            adminService.deleteByUsername(username);
        } catch (UserDoesNotExists userDoesNotExists) {
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/admin/userlist/{username}-{role}", method = RequestMethod.POST)
    public ResponseEntity<Void> changeRole(
            @PathVariable("username") String username,
            @PathVariable("role") String role) throws UserDoesNotExists {
        adminService.changeRole(username, role);

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/admin/modify", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<Void> modify(Principal principal, @RequestBody String body) throws DataBaseException {
        Map<String, Object> userDataMap = new HashMap<>();

        try {
            JsonNode jsonNode = objectMapper.readTree(body);
            User userSelected = adminService.getSelectedUsername(jsonNode);
            userService.modify(userSelected.getUsername(), jsonNode);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (DataBaseException dataBaseException) {
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
