package com.bootcamp.homework.web;

import com.bootcamp.homework.business.service.exceptions.UserAlreadyExists;
import com.bootcamp.homework.business.service.exceptions.UserDoesNotExists;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@Controller
public class ErrorController {

    @RequestMapping(value = "/templates/error/errorPage", method = RequestMethod.GET)
    public String error() {
        return "errorPage";
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "The given data cannot be interpereted.")
    public void handle(MethodArgumentTypeMismatchException e) {
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "The specified user does not exists")
    public void handle(UserDoesNotExists e) {
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.CONFLICT, reason = "The specified user already exists")
    public void handle(UserAlreadyExists e) {
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "The specified user cannot be found")
    public void handle(DataRetrievalFailureException e) {

    }
}
